This tiny project is a mostly-header library which provides several useful
types and utilities which I tend to reinvent on each next project

TODO:

* Logging facade
* Option type. Unlike C++17 one, supports sentinel values.
* Reference-wrapping type. Unlike std::reference_wrapper, cannot be constructed
  implicitly, has shorter name, tends to be immutable, and can refer to any
  callable object
* Span - same as std::span, which is still not in STL
* StrSpan - similar to std::basic_string_view, but derived from Span